﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bac.Models
{
    public class Marcas
    {
        [Key]
        public int id_marca { get; set; }
        [Required]
        public string marca { get; set; }
    }
}
