﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bac.Models
{
    public class Colores
    {
        [Key]
        public int id_color { get; set; }
        [Required]
        public string color { get; set; }
    }
}
