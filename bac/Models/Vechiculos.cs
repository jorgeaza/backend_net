﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bac.Models
{
    public class Vechiculos
    {
        [Key]
        public int id_vehiculo { get; set; }
        [Required]
        public string descripcion { get; set; }
        [Required]
        public string matricula { get; set; }
        [Required]
        public string precio { get; set; }
        [Required]
        public int tipo_vehiculo { get; set; }
        [Required]
        public int color { get; set; }
        [Required]
        public int marca { get; set; }


    }
}
