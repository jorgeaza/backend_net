﻿using bac.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace bac.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiculoController : ControllerBase
    {
        private readonly AplicationDbContext _context;
        public VehiculoController(AplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/<VehiculoController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {

                var listVehiculos = await _context.Vehiculos.ToListAsync();
                return Ok(listVehiculos);

            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        // GET api/<VehiculoController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {

                var vehiculo = await _context.Vehiculos.FindAsync(id);

                if (vehiculo == null)
                {
                    return Ok(vehiculo);
                }

                return Ok(vehiculo);

            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        // POST api/<VehiculoController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Vechiculos vechiculos)
        {
            try
            {


                _context.Add(vechiculos);
                await _context.SaveChangesAsync();

                return Ok(vechiculos);

            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        // PUT api/<VehiculoController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Vechiculos vechiculos)
        {

            try
            {
                if (id != vechiculos.id_vehiculo)
                {

                    return BadRequest();

                }

                _context.Update(vechiculos);
                await _context.SaveChangesAsync();
                return Ok(new { mensaje = "comentario actualizado con exito!" });

            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<VehiculoController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
